package com.app.countryinformation.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.countryinformation.R;
import com.app.countryinformation.Utils;
import com.app.countryinformation.data.models.CountryResponse;

import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class CountriesAdapter  extends RecyclerView.Adapter<CountriesAdapter.ViewHolder> {

    private final List<CountryResponse> lsItems;
    private final OnActionListener mListener;

    public interface OnActionListener{
        void onItemSelected(CountryResponse country);
    }

    public CountriesAdapter(List<CountryResponse> lsItems, OnActionListener mListener) {
        this.lsItems = lsItems;
        this.mListener = mListener;
        Collections.reverse(lsItems);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_list_countries, null);

        return new ViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CountryResponse model = lsItems.get(position);
        if (model != null) {
            if (model.getName() != null) {
                holder.tvName.setText(model.getName());
            }

            if (model.getCapital() != null) {
                holder.tvCapitol.setText(model.getCapital());
            }

            if (model.getFlag() != null) {
                Utils.fetchSvg(holder.itemView.getContext(), model.getFlag(), holder.ivFlag);
            }
            holder.container.setOnClickListener(new OnItemSelected(mListener, model));
        }
    }

    @Override
    public int getItemCount() {
        return lsItems.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        final TextView tvName;
        final TextView tvCapitol;
        final ImageView ivFlag;
        final RelativeLayout container;

        ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.text_name);
            tvCapitol = itemView.findViewById(R.id.text_capitol);
            ivFlag = itemView.findViewById(R.id.image_flag);
            container = itemView.findViewById(R.id.item_country_container);
        }
    }

    private static class OnItemSelected implements View.OnClickListener{

        private final WeakReference<CountriesAdapter.OnActionListener> listener;
        private final WeakReference<CountryResponse> item;

        OnItemSelected(CountriesAdapter.OnActionListener l, CountryResponse item) {
            listener = new WeakReference<OnActionListener>(l);
            this.item = new WeakReference<CountryResponse>(item);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.item_country_container:
                    CountriesAdapter.OnActionListener l = listener.get();
                    CountryResponse i = item.get();

                    if(l != null && i != null){
                        l.onItemSelected(i);
                    }
                    break;
            }
        }
    }
}
