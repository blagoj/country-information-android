package com.app.countryinformation.activities;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;

import com.app.countryinformation.App;
import com.app.countryinformation.R;
import com.app.countryinformation.adapters.CountriesAdapter;
import com.app.countryinformation.api.ApiService;
import com.app.countryinformation.data.models.CountryResponse;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

public class MainActivity extends Activity implements TextWatcher, AdapterView.OnItemClickListener {

    CountriesAdapter countriesAdapter;

    @Inject
    public ApiService apiService;

    private Unbinder mBinder;
    @BindView(R.id.rvlist_list_countries)
    RecyclerView rv;

    List<CountryResponse> listCountries;

    @BindView(R.id.f_search_auto_complete)
    AutoCompleteTextView autoComplete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mBinder =  ButterKnife.bind(this);
        setComponents();
    }

    private void setListeners(){
        autoComplete.addTextChangedListener(this);
    }

    private void setComponents() {
        ((App) getApplication()).getComponent().inject(this);
        setListeners();
        getListCountries();
    }

    /**
     * Used for search some country
     */
    @Override
    public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
        if(count>2){
            getCountry(s.toString());
        }
        autoComplete.setOnItemClickListener((parent, view, position, id) -> {
            getCountry(s.toString());
        });
    }

    @Override
    public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {
        System.out.println("OnItemClick.");
    }

    @Override
    public void beforeTextChanged(final CharSequence s, final int start, final int count, final int after) {
    }

    @Override
    public void afterTextChanged(final Editable s) {

    }

    /**
     * Build the 'Create country detail' request. Used to get country
     *
     * @return array(String)
     * @throws \Exception
     */
    private void getListCountries(){
        listCountries = new ArrayList<>();;
        apiService.getAllCountries().enqueue(new Callback<List<CountryResponse>>() {
            @Override
            public void onResponse(@NonNull Call<List<CountryResponse>> call, @NonNull Response<List<CountryResponse>> response) {
                if(response.code() == 200){
                    listCountries.addAll(Objects.requireNonNull(response.body()));
                    getAllCountries();
                }else {
                    System.out.println("Response error:" + response.code());
                }
            }
            @Override
            public void onFailure(@NonNull Call<List<CountryResponse>> call, @NonNull Throwable t) {
                t.getMessage();
            }
        });
    }

    private void getAllCountries() {
        RecyclerView.LayoutManager lm = new LinearLayoutManager(this);
        rv.setLayoutManager(lm);

        countriesAdapter = new CountriesAdapter(listCountries, new OnItemActionListener(this));
        rv.setAdapter(countriesAdapter);
    }

    private void getCountry(List<CountryResponse> country) {
        RecyclerView.LayoutManager lm = new LinearLayoutManager(this);
        rv.setLayoutManager(lm);

        countriesAdapter = new CountriesAdapter(country, new OnItemActionListener(this));
        rv.setAdapter(countriesAdapter);
    }

    /**
     * Build the 'Create list of countries' request. Used to get countries
     *
     * @return array(List<string>)
     * @throws \Exception
     * @param country name
     */
    private void getCountry(String name){
        apiService.getCountry(name).enqueue(new Callback<List<CountryResponse>>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<List<CountryResponse>> call, Response<List<CountryResponse>> response) {
                if(response.code() == 200){
                    getCountry(response.body());
                }else {
                    System.out.println("Response code:" + response.code());
                }
            }
            @Override
            public void onFailure(Call<List<CountryResponse>> call, Throwable t) {
                t.getMessage();
            }
        });
    }

    private static class OnItemActionListener implements CountriesAdapter.OnActionListener{
        final WeakReference<MainActivity> weakReference;

        final MainActivity ac;

        OnItemActionListener(MainActivity activity) {
            weakReference = new WeakReference<MainActivity>(activity);
            ac=weakReference.get();
        }

        @Override
        public void onItemSelected(CountryResponse country) {
                Intent i = new Intent(ac, CountryDetailsActivity.class);
                i.putExtra("name", country.getName());
                ac.startActivity(i);
        }
    }
}
