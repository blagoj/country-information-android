package com.app.countryinformation.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.countryinformation.App;
import com.app.countryinformation.R;
import com.app.countryinformation.api.ApiService;
import com.app.countryinformation.data.models.CountryResponse;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CountryDetailsActivity extends AppCompatActivity implements OnMapReadyCallback {

    @Inject
    public ApiService apiService;

    private Unbinder unbinder;

    @BindView(R.id.country_flag)
    ImageView countryFlag;

    @BindView(R.id.country_capital)
    TextView countryCapital;

    @BindView(R.id.country_name)
    TextView countryName;

    @BindView(R.id.country_numeric_code)
    TextView countryNumericCode;

    @BindView(R.id.country_population)
    TextView countryPopulation;

    @BindView(R.id.country_region)
    TextView countryRegion;

    private GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_details);
        unbinder = ButterKnife.bind(this);
        ((App) getApplication()).getComponent().inject(this);

        init();
    }

    private void init(){
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_view);
        Objects.requireNonNull(mapFragment).getMapAsync(this);

        if(getIntent().getExtras().getString("name") != null){
            getCountry(getIntent().getExtras().getString("name"));
        }
    }

    /**
     * Build the 'Create list of countries' request. Used to get countries
     *
     * @return array(List<string>)
     * @throws \Exception
     * @param country name
     */
    private void getCountry(String name){
        apiService.getCountry(name).enqueue(new Callback<List<CountryResponse>>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<List<CountryResponse>> call, Response<List<CountryResponse>> response) {
                if(response.code() == 200){
                    countryCapital.setText("Capitаl city: "+response.body().get(0).getCapital());
                    countryNumericCode.setText("numeric code: "+response.body().get(0).getNumericCode());
                    String population = String.valueOf(response.body().get(0).getPopulation());
                    countryPopulation.setText("country population: " + population);
                    countryName.setText("country name: "+ response.body().get(0).getName());
                    countryRegion.setText("country region: "+response.body().get(0).getRegion());
                    double lat = response.body().get(0).getLatlng().get(0);
                    double lon = response.body().get(0).getLatlng().get(1);
                    initMap(googleMap,lat, lon, name);
                }else {
                    System.out.println("Response error:" + response.code());
                }
            }
            @Override
            public void onFailure(Call<List<CountryResponse>> call, Throwable t) {
                t.getMessage();
            }
        });
    }

    /**
     * Build the 'Create list of countries' request. Used to find country on google map
     *
     * @param  googleMap
     * @param  latitude
     * @param  longitude
     * @param country name
     */
    private void initMap(GoogleMap googleMap,double lat,double lon, String name) {
        try {
            LatLng latLng = new LatLng(lat, lon);
            googleMap.addMarker(new MarkerOptions().position(latLng).title(name));
            CameraUpdate center = CameraUpdateFactory.newLatLng(latLng);
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(14);
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            googleMap.moveCamera(center);
            googleMap.animateCamera(zoom);
            googleMap.setOnMapClickListener(new OnMapClickListener(this));
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(this, "Error initializing location on map", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
    }

    @OnClick(R.id.btnBack)
    void onButtonBackClick(){
        finish();
    }

    @OnClick(R.id.share_btn)
    void onButtonShareClick(){
        shareCountry();
    }

    /**
     * Uset to share country
     *
     */
    private void shareCountry() {
        String text = "Hey, I'm going to join me!";
        PackageManager pm = getPackageManager();
        Intent basicIntent = new Intent();
        basicIntent.setAction(Intent.ACTION_SEND);
        basicIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        basicIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, text);
        basicIntent.setType("text/plain");
        List<ResolveInfo> resInfo = pm.queryIntentActivities(basicIntent, 0);

        startActivity(Intent.createChooser(basicIntent, "Share"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    private static class OnMapClickListener implements GoogleMap.OnMapClickListener {
        final WeakReference<CountryDetailsActivity> venueInfoActivityWeakReference;

        OnMapClickListener(CountryDetailsActivity activity) {
            venueInfoActivityWeakReference = new WeakReference<>(activity);
        }

        @Override
        public void onMapClick(LatLng latLng) {

        }
    }
}
