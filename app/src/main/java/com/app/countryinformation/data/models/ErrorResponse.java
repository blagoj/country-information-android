package com.app.countryinformation.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ErrorResponse {

    @SerializedName("Key")
    @Expose
    private String key;
    public String getKey() {
        if(key == null)
            return "";
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
