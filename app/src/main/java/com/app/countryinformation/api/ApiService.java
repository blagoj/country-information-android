package com.app.countryinformation.api;

import com.app.countryinformation.data.models.CountryResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiService {

    @GET("all")
    Call<List<CountryResponse>> getAllCountries();

    @GET("name/{name}")
    Call<List<CountryResponse>> getCountry(@Path("name") String name);
}
