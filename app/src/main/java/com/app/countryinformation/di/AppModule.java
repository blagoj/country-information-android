package com.app.countryinformation.di;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.app.countryinformation.BuildConfig;
import com.app.countryinformation.api.ApiService;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

@Module
public class AppModule {
    private Application applicationContext;


    public AppModule(Application  context){
        this.applicationContext = context;
    }

    @Provides
    @Singleton
    public Context getContext(){
        return applicationContext;
    }

    @Provides
    @Singleton
    public Retrofit getRetrofit(){
        return new Retrofit.Builder()
                .client(authClient().build())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .baseUrl(BuildConfig.BASE_URL)
                .build();
    }

    private static OkHttpClient.Builder authClient() {

        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.connectTimeout(30, TimeUnit.SECONDS);
        client.readTimeout(30, TimeUnit.SECONDS);
        client.writeTimeout(30, TimeUnit.SECONDS);
        client.followSslRedirects(true);
        client.followRedirects(true);
        client.connectionPool(new ConnectionPool(0, 1, TimeUnit.NANOSECONDS));
        client.retryOnConnectionFailure(true);

        return client;
    }
    public static Gson gson() {
        return new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).setLenient().create();
    }

    @Provides
    @Singleton
    public ApiService create(){
        return getRetrofit().create(ApiService.class);
    }

    @Provides
    @Singleton
    public SharedPreferences getSharedPreferences(){
        return PreferenceManager.getDefaultSharedPreferences(applicationContext);
    }

}


