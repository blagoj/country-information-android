package com.app.countryinformation.di;

import com.app.countryinformation.activities.CountryDetailsActivity;
import com.app.countryinformation.activities.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {
    void inject(MainActivity mainActivity);
    void inject(CountryDetailsActivity countryDetailsActivity);
}
