package com.app.countryinformation;

import android.app.Application;

import com.app.countryinformation.di.AppComponent;
import com.app.countryinformation.di.AppModule;
import com.app.countryinformation.di.DaggerAppComponent;
import com.facebook.stetho.Stetho;

public class App extends Application {

    private AppComponent component;


    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);

        component = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    }

    public AppComponent getComponent() {
        return component;
    }
}
